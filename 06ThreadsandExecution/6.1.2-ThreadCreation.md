### 6.1.2 线程创建

与其他对象一样，TCB是使用seL4_Untyped_Retype()方法创建的(参见第2.4节)。新创建的线程最初是不活跃的，要通过调用seL4_TCB_SetSpace()或seL4_TCB_Configure()方法设置它的CSpace和VSpace，然后调用seL4_TCB_WriteRegisters()来配置它的初始堆栈和指令指针。之后，就可以通过将seL4_TCB_WriteRegisters()调用中的resume_target参数设置为true，或单独调用seL4_TCB_Resume()方法来激活线程。这两个方法都将线程置于可运行状态。

在主线内核上，这将使线程立即被添加到调度器中。而在MCS内核上，线程只有在拥有调度上下文对象时才会开始运行。

如果配置了SMP内核选项，线程将在与其亲和cpu相符的核上运行。对于主线内核，亲和cpu通过seL4_TCB_SetAffinity()设置，而在MCS内核，亲和性由调度上下文对象中派生。
