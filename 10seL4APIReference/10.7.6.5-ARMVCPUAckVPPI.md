#### 10.7.6.5  AckVPPI

static inline int seL4_ARM_VCPU_AckVPPI

应答之前因VPPIEvent错误而转发的PPI中断。

类型 | 名字 | 描述
--- | --- | ---
seL4_ARM_VCPU | _service | 引用的VCPU能力句柄。自当前线程根CNode按机器字位数解析
seL4_Word | irq | 要应答的irq号

*返回值*：返回0表示成功，非0值表示有错误发生。第10.1节描述了发生错误时消息寄存器和消息标签有关内容。

*描述*：应答PPI中断，并解除屏蔽以便可以继续通过VPPIEvent错误转发中断。
